import paho.mqtt.client as mqttClient
import mysql.connector
import time
import json
import toml
from model import *

with open('config.toml') as f1:
    config = toml.loads(f1.read())
Connected = False
BROKER_ENDPOINT = config['MQTT_SERVER']['HOST']
PORT = config['MQTT_SERVER']['PORT']
MQTT_USERNAME = config['MQTT_SERVER']['USERNAME'] 
MQTT_PASSWORD = config['MQTT_SERVER']['PASSWORD']
TOPIC=config['APPLICATION']['TOPIC']
MQTTdb=None
DATBASENAME=config['MQTTDB']['DB']
DATBASEUSER=config['MQTTDB']['DBUSER']
DATBASEPASSWORD=config['MQTTDB']['DBPASSWORD']
DATBASEHOST=config['MQTTDB']['HOST']
def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass
def on_message(client, userdata, message):
    global MQTTdb
    DATA=json.loads(message.payload)
    DATA=json.loads(DATA['payload'])

    appid=DATA['applicationID']
    appname=DATA['applicationName']
    devicename=DATA['deviceName']
    deviceeui=DATA['devEUI']
    password="ICFOSS::"+str(appid)+'-'+appname+"_"+devicename
    try:
        dataobject=DATA['object']
    except:
        dataobject={"error":"payload not decode"}
    pubdata=json.dumps({"payload":dataobject,"deveui":deviceeui})
    if isuserexist(MQTTdb, deviceeui):
        if checkpassword(MQTTdb, password, deviceeui):
            print("user ok")
            client.publish(deviceeui,   pubdata)
        else:
            modifiuser(MQTTdb,password,deviceeui)
            print("user modified")
    else:
        insertuser(MQTTdb,password,deviceeui)
        print("useradded")
    

def on_connect(client, userdata, flags, rc):
    global MQTTdb
    if rc == 0:
        print("Connected to LOCAL BROKER")
        global Connected                #Use global variable
        Connected = True                #Signal connection 
        MQTTdb = mysql.connector.connect(host=DATBASEHOST,user=DATBASEUSER,password=DATBASEPASSWORD,database=DATBASENAME)
        print(MQTTdb)
    else:
        print("Connection failed")
mqtt_client = mqttClient.Client(client_id=MQTT_USERNAME,)
mqtt_client.username_pw_set(MQTT_USERNAME, password=MQTT_PASSWORD)
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message
mqtt_client.on_publish = on_publish
mqtt_client.connect(BROKER_ENDPOINT, port=PORT)
mqtt_client.loop_start()       
 
while Connected != True:    
    time.sleep(0.1)
 
mqtt_client.subscribe(TOPIC)
    
try:
    while True:
        time.sleep(1)
 
except KeyboardInterrupt:
    print ("exiting")
    mqtt_client.disconnect()
    mqtt_client.loop_stop()
      